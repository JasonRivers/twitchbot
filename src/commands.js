const permissions = require('./permissions');
const cooldown = require('./cooldown');

module.exports = {
  execCommand: (userstate, command, args, c) => {
    var userBadges = [];
    if (userstate.badges) {
      userBadges = Object.keys(userstate.badges);
    }

//    if (cooldown.checkCooldown(userstate, command)) {
//      return false;
//    } else {
//      cooldown.addCooldown(userstate,command, 60, 60);
//    }


    // search modules for commands
    moduleNames = Object.keys(global.modules);
    for (i=0; i<moduleNames.length; i++) {
      if (global.modules[moduleNames[i]].commands) {
        global.modules[moduleNames[i]].commands(userstate, userBadges, command, args, (result) => {
          c(result);
        });
      }
    }
  }
}