const https = require('https');
const api = require('twitch-api-v5');
const logger = require('./logger');
const { loadModules } = require('./modules');
api.clientID = "j7qmgyvnkefvwdm2ccrae3fafgaztw";

module.exports = {
    helixGet: (path, c) => {
        global.oauth.checkToken('twitch', () => {
        const options = {
            hostname: 'api.twitch.tv',
            port: 443,
            path: '/helix/' + path,
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Client-ID': process.env.TWITCH_CLIENT_ID,
                'Authorization': 'Bearer '+global.app.config.twitch.access_token
            }
        }
        var req = https.request(options, (res) => {
            let chunks = [];
            res.on('data', (d) => {
                chunks.push(d);
            }).on('end', () => {
                data = Buffer.concat(chunks);
                try {
                    let json_return = JSON.parse(data);
                    c(json_return);
                }
                catch (e) {
                    logger.error(e);
                    c(false);
                }
            })
        })
        req.on('error', (e) => {
            logger.error(e);
        })
        req.end();
        });
    },
    krakenGet: (path, c) => {
        logger.log('*** WARNING: Depricated call to Kraken API ***');
        var stack = new Error().stack;
        logger.debug(stack);
        global.oauth.checkToken('twitch', () => {
        const options = {
            hostname: 'api.twitch.tv',
            port: 443,
            path: '/kraken/' + path,
            method: 'GET',
            headers: {
                'Accept': 'application/vnd.twitchtv.v5+json',
                'Content-Type': 'application/json',
                'Client-ID': process.env.TWITCH_CLIENT_ID,
                'Authorization': 'Bearer '+global.app.config.twitch.access_token
            }
        }
        var req = https.request(options, (res) => {
            let chunks = [];
            res.on('data', (d) => {
                chunks.push(d);
            }).on('end', () => {
                data = Buffer.concat(chunks);
                try {
                    let json_return = JSON.parse(data);
                    c(json_return);
                }
                catch (e) {
                    logger.error(e);
                    c(false);
                }
            })
        })
        req.on('error', (e) => {
            logger.error(e);
        })
        req.end();

        });
    },
    userByName: (channelName, closure) => {
        module.exports.helixGet('users?login=' + channelName, (result) => {
//            if (err) {
//                logger.error(err);
//            }
logger.debug("userbyname");
logger.debug(result);
            if (result && result.data[0]) {
                closure(result.data[0]);
            }
        });
    },
    channelByID: (channelID, closure) => {
        api.channels.channelByID({channelID: channelID}, (err,result) => {
            if (err) {
                logger.error(err);
            }
            closure(result);
        });
    },
    streamByChannelID: (channelID, closure) => {
        api.streams.channel({channelID: channelID}, (err, result) => {
            if (err) {
                logger.error(err);
            }
            if (result && result.stream) {
                closure(result.stream);
            } else {
                closure(false);
            }
        })
    },
    getGameFromUser: (username, c) => {
        module.exports.userByName(username, (user) => {
            module.exports.channelByID(user[0]._id, (channel) => {
                c(channel.game);
            })
        })
    },
    getTwitchClips: (options, c) => {
        username = options.username;
        logger.debug(options);
        if (options && options.startdate) {
            startdate = options.startdate;
        }

        module.exports.userByName(username, (u) => {

            logger.debug("username");
            logger.debug(u);
            module.exports.helixGet('clips?broadcaster_id=' + u.id + '&started_at=' + startdate, (clips) => {
                console.log(clips);
                c(clips);
            })

        })
    },
    getTeamMembers: (team, c) => {
        module.exports.helixGet('teams/?name=' + team, (r) => {
            let members=[]
            for (var i=0; i<r.data[0].users.length; i++) {
                members.push(r.data[0].users[i].user_name);
            }
            c(members);
        });
    }
}