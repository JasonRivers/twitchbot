const app = require('express')();
const http = require('http').createServer(app);
const path = require('path');
const bodyParser = require('body-parser');
var querystring = require('querystring');
var request = require('request')
const open = require('open');
//const { logger } = require('./logger');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());

var generateRandomString = function(length) {
  var text = '';
  var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
};

twitchOptions = {
  authURL: "https://id.twitch.tv/oauth2/authorize?",
  authURL2: "https://id.twitch.tv/oauth2/token?",
  redirect_uri: "http://localhost:6501/oauth/callback/twitch",
  scope: "chat:read chat:edit user_read bits:read channel:read:redemptions channel_subscriptions",
}

module.exports = {
  init: () => {
    logger.log('Starting HTTP UI Services');

    twitchOptions.client_id = process.env.TWITCH_CLIENT_ID;
    twitchOptions.client_secret = process.env.TWITCH_CLIENT_SECRET;

    //oauth stuff
    app.get('/oauth/:service', (req, res) => {
        service = req.params.service;
        switch (service) {
            case 'twitch':
              options=twitchOptions;
              break;
        }
        redirect = options.authURL +
        querystring.stringify({
            response_type: 'code',
            client_id: options.client_id,
            scope: options.scope,
            redirect_uri: options.redirect_uri,
            state: generateRandomString(16)
        });
        logger.debug(redirect);
        res.redirect(redirect);

    });
    app.get('/oauth/callback/:service', (req, res) => {
      service = req.params.service;
      logger.debug('Callback for ' + service + ' authentication')
        var code = req.query.code;
        var authOptions;
        switch (service) {
            case 'twitch':
              authOptions = {
                url: twitchOptions.authURL2,
                form: {
                  code,
                  redirect_uri: twitchOptions.redirect_uri,
                  grant_type: 'authorization_code',
                  client_id: twitchOptions.client_id,
                  client_secret: twitchOptions.client_secret
                },
                json: true
              }
              break;
        }
      module.exports.getoAuthToken(service, res, authOptions)
    })
    http.listen(global.app.config.webserver.port, () => {
        logger.log('OAUTH HTTP Listening on port ' + global.app.config.webserver.port);    
    }).on('error', function(e) {
        logger.error(e);
    });
  },
  getoAuthToken: (service, res, authOptions) => {
    logger.log('**** Getting oauth code for ' + service + ' ****')
    request.post(authOptions, function(error, response, body) {
      if (error) {
        logger.error(error);
      }
      if (body && body.status && body.status === 400) {
        res.send(body.message);
        logger.error(body.message);
        return;
      }
      if (body && body.error) {
        logger.error(body.error);
      }

      var seconds = Math.floor(new Date().getTime() / 1000);
      seconds = seconds + parseInt(body.expires_in);
      if (!global.app.config[service]) {
        global.app.config[service] = {};
      }
      logger.debug(body);

      global.app.config[service].access_token=body.access_token;
      global.app.config[service].refresh_token=body.refresh_token;
      global.app.config[service].expires_at=seconds;
      logger.log(global.app.config[service]);

      global.writeConfig(service);
      logger.debug('data saved..... maybe')
      res.send('<script>window.close()</script>');


      if (global.app && global.app.modules && global.app.modules[service] && global.app.modules[service].init) {
        global.app.init();
//        eval('global.app.modules.' + service + '.init()');
//        global.app.modules.pubsub.init();
      }
    })
  },
  refreshToken: (service, c) => {
    logger.log ("************** Refresh " + service + " token *************")

    switch (service) {
      case "twitch":
        options = twitchOptions;
        authOptions = {
        url: options.authURL2,
        form: {
          grant_type: 'refresh_token',
          refresh_token: global.config.twitch.refresh_token,
          client_id: options.client_id,
          client_secret: options.client_secret
        },
        headers: {
          Accept: 'application/json'
        },
        json: true
      }
        break;
    }
      //logger.debug(global.config[service]);
      //logger.debug(authOptions);
    request.post(authOptions, (err, response, body) => {
        logger.debug("Got refresh data");
      if (err) {
        logger.error(err)
        return false;
      }
      //logger.debug(body);
      //logger.debug('access token:' + body.access_token);
      if (body && body.access_token) {
        logger.debug('setting new auth');
        var seconds = Math.floor(new Date().getTime() / 1000);
        seconds = seconds + body.expires_in;
        global.config[service].access_token = body.access_token;
        global.config[service].expires_at = seconds;
        logger.debug(global.config[service]);
        global.writeConfig(service);
        if (c) {
          c(true);
        }
      } else  {
        if (c) {
          c(false);
        }
      }

    })
  },
  checkToken: (service, c) => {
    var seconds = Math.floor(new Date().getTime() / 1000);
    if (!global.config[service] || ! global.config[service].access_token) {
      open('http://localhost:6501/oauth/' + service);
    } else 
      if (global.config[service].expires_at && global.config[service].expires_at < seconds) {
        logger.log('renewing token');
        module.exports.refreshToken ('twitch', c);
      } else {
        logger.debug ('Twitch token expires in '+ (global.config[service].expires_at - seconds)+' seconds');
      if (c) {
        c();
      }
    }
  }
}