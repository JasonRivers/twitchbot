const fs = require('fs');

async function getConfig() {
  try {
    config={};
    files = await fs.readdirSync('./config/');
    for (i=0; i < files.length; i++) {
      if(files[i].includes('.json')) {
        configName=files[i].replace('.json', '');
        config[configName]= await JSON.parse(fs.readFileSync('./config/' + files[i]));
      }
    }
//    config.myChannel = config.twitch.twitchBot.channels[0];
    return(config);

  } catch (err) {
    console.error('Error occured while reading directory!', err);
  }
}

async function storeConfig(config) {
  var configFile = config +'.json';
  var __config = JSON.stringify(global.app.config[config], null, 2);
  try {
    fs.writeFileSync('./config/' +configFile, __config, {
      encoding: 'utf8'
    });
  }
  catch (e) {
    logger.error(e);
  }
}

module.exports = {
  getConfig,
  storeConfig
}