module.exports = {
    globalCooldown: (command) => {
        currentTime = new Date();
        currentTime = Math.floor(currentTime / 1000);
        if (global.cooldown && global.cooldown[command] && global.cooldown[command].global) {
            if (global.cooldown[command].global > currentTime) {
                logger.debug('in Global cooldown')
                return true;
            }
        }
        return false;
    },
    userCooldown: (command, userstate) => {
        if (global.cooldown && global.cooldown[command] && global.cooldown[command].users) {
            logger.debug(global.cooldown[command].users);
            cooldown = global.cooldown[command].users;
        } else {
            return false;
        }
        currentTime = new Date();
        currentTime = Math.floor(currentTime / 1000);
        for (var i=0; i<cooldown.length; i++) {
            if (cooldown[i].username === userstate.username) {
                if (cooldown[i].cooldown > currentTime) {
                    logger.debug('user in cooldown')
                    return true;
                }
            }
        }
        return false;
    },
    checkCooldown: (userstate, command) => {
        command = command.replace('!', '');
        logger.debug(global.cooldown);
        cooldown_g = module.exports.globalCooldown(command);
        cooldown_u = module.exports.userCooldown(command, userstate);
        if (!cooldown_g || !cooldown_u) {
            logger.debug('not in cooldown')
            return false;
        } else {
            logger.debug('cooldown')
            return true;
        }
    },
    addCooldown: (userstate, command, usercooldown, globalcooldown) => {
        command = command.replace('!', '');
        if (!global.cooldown) {
            global.cooldown = {};
        }
        if (!global.cooldown[command]) {
            global.cooldown[command] = {};
        }
        currentTime = new Date();
        currentTime = Math.floor(currentTime / 1000);
        g_cooldown = currentTime + (globalcooldown);
        u_cooldown = currentTime + (usercooldown);
        if (!global.cooldown[command].users) {
            global.cooldown[command].users = [];
        }
        user_CD = {
            username: userstate.username,
            cooldown: u_cooldown
        }

        global.cooldown[command].global = g_cooldown;
        global.cooldown[command].users.push(user_CD);
    }
}