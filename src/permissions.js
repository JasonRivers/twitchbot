module.exports = {
  checkPermissions: (userBadges, commandPermissions) => {
    if (commandPermissions[0] === 'everyone') {
      return true;
    }
    for (var p=0; p<userBadges.length; p++) {
      if (commandPermissions.indexOf(userBadges[p]) > -1) {
        return true;
      }
    }
    return false;
  }
}
