module.exports = {
    stream: (closure) => {
        twitchapi.userByName(global.status.myChannel, (user) => {
            twitchapi.streamByChannelID(user[0]._id,(stream) => {
                if (stream) {
                    global.status.isLive = true;
                } else {
                    global.status.isLive = false;
                }
                closure(global.status.isLive);
            });
        });
    },
    getStreamTitle: (closure) => {
        twitchapi.userByName(global.status.myChannel, (user) => {
            twitchapi.channelByID(user[0]._id,(stream) => {
                closure(stream.status);
            })
        })

    },
    game: (channel, closure) => {
        twitchapi.userByName(channel, (user) => {
            twitchapi.channelByID(user[0]._id, (channel) => {
                closure(channel.game);
            })
        })
    },
    uptime: (closure) => {
        var uptime = {
            hours: 0,
            minutes: 0
        }
        if (global.status.isLive && global.status.startTime !== 0) {
            uptimeSeconds = (new Date().getTime() / 1000) - global.status.startTime;
            uptime.hours = Math.floor(uptimeSeconds / 3600);
            uptime.minutes = Math.floor(uptimeSeconds % 3600 / 60);
        }
        closure(uptime);
    }

}