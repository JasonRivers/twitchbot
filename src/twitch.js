const tmi = require('tmi.js');
const commands = require('./commands');
const permissions = require('./permissions');
const api = require('twitch-api-v5');
var twitchBot;

var cooldown=[];

function getGameFromUser(username, c) {
  api.clientID = process.env.TWITCH_CLIENT_ID;

  api.users.usersByName({users: username}, (err, result) => {
    if (err) {
      logger.error('usersByName Error');
      logger.error(err);
    } else {
      if (result.users) {
        api.channels.channelByID({
          channelID: result.users[0]._id
        }, (err, channelRes) => {
          if (err) {
            logger.error('channelByID Error');
            logger.error(err);
          } else {
            c(channelRes.game)
          }
        })
      }
    }
  })
}


function onChatHandler(channel, userstate, message, self) {
  if (self) return
    // Get the command
    var command = message.replace(/ .*/, '').toLowerCase();
    var args = message.replace(/[^\s]+ /, '').split(' ');


    let moduleNames =  Object.keys(global.modules);
    for (var i=0; i<moduleNames.length; i++) {
      if (global.modules[moduleNames[i]].onChatHandler) {
        global.modules[moduleNames[i]].onChatHandler(channel, userstate, message, self);
      }
    }
    // check cooldown
    if (userstate && userstate.badges) {
      userBadges = Object.keys(userstate.badges);
    } else {
      userBadges = [];
    }
    if (permissions.checkPermissions(userBadges, ['moderator', 'broadcaster'])) {
      if (command === "!trip" || command === "!trippy") {
        setTrippyScene(10);
        return;
      }
    }
    if (permissions.checkPermissions(userBadges, ['subscriber', 'founder', 'moderator', 'broadcaster'])) {
      if (command === "!beard"){
        setTrippyScene(5);
        twitch.say('!wadmin wobble 360');
        setTimeout(() => {
          twitch.say('!wadmin set base rot 0 180 0');
          twitch.say('!wadmin clear all');
          setTimeout(() => {
            setTrippyScene(5);
            twitch.say('!wadmin wobble -360');
            setTimeout(() => {
              twitch.say('!wadmin clear all');
              twitch.say('!wadmin set base rot 0 0 0');
            },5000)
          },20000)
        },5000)
        return;
      }
    }

    commands.execCommand (userstate, command, args, (message) => {
      twitchBot.say('#mohero_', message);
    });
}

function setTrippyScene(timer=20) {
  global.modules.obs.setItemVisible('Render - Trip', 'Trip-NDI', true, () => {
    setTimeout(() => {
      global.modules.obs.setItemVisible('Render - Trip', 'Trip-NDI', false, () => {
        logger.log('Trip completed');
      });
    }, (timer * 1000))
  });
//  global.modules.obs.getScene((sceneName) => {
//    logger.log(sceneName);
//    if (sceneName === "Beatsaber" || sceneName === "BeatsaberHMD") {
//      global.modules.obs.setScene('Beatsaber-trip', () => {
//        setTimeout(() => {
//          global.modules.obs.setScene('Beatsaber');
//        }, (timer * 1000))
//      })
//    }
}

module.exports = {
  getGameFromUser,
  init: () => {
   //global.status.myChannel = global.config.twitch.twitchBot.channels[0];
   //twitchBroadcaster = new tmi.client(global.config.twitch.twitchBroadcaster) ;
   twitchbotOptions = {
    options: {
      debug: true,
      clientId: process.env.TWITCH_CLIENT_ID
    },
    connection: {
      reconnect: true
    },
    identity: {
      username: "mohero_bot",
      password: "oauth:ujojgtnuyr23ci568n84xjmk2qolew"
    },
    channels: ["#mohero_"]
  };
  twitchBroadcasterOptions = {
    options: {
      debug: true,
      clientId: process.env.TWITCH_CLIENT_ID
    },
    connection: {
      reconnect: true
    },
    identity: {
      username: "mohero_",
      password: global.config.twitch.access_token
    },
    channels: ["#mohero_"]
  }   

   global.status.myChannel = twitchbotOptions.channels[0].replace('#','');

   twitchBroadcaster = new tmi.client(twitchBroadcasterOptions) ;
   twitchBot = new tmi.client(twitchbotOptions) ;
   twitchBot.connect();
   twitchBroadcaster.connect();

   twitchBot.on('connected', () => {
     twitchBot.say('#'+global.status.myChannel, global.application.displayName + ' (' + global.application.version + ') is connected');
     logger.log("Twitch Connected");
   })

   twitchBot.on('chat', onChatHandler);
   twitchBot.on('raided', () => {
     logger.log('raid');
    setTrippyScene(20)
   } );
   twitchBot.on("anongiftpaidupgrade", () => {
     logger.log('sub');
     setTrippyScene(10);
   } );
   twitchBot.on("giftpaidupgrade", () => {
     logger.log('sub');
     setTrippyScene(10);
   });
   twitchBot.on("resub", () => {
     logger.log('sub');
     setTrippyScene(10);
   } );
   twitchBot.on("subgift", () => {
     logger.log('sub');
     setTrippyScene(10);
   } );
   twitchBot.on("submysterygift", () => {
     logger.log('sub');
     setTrippyScene(10);
   } );
   twitchBot.on("subscription", () => {
     logger.log('sub');
     setTrippyScene(10);
   } )
   twitchBot.on("cheer", (c, userstate, msg) => {
     logger.log('cheer');
     if (userstate && userstate.bits && userstate.bits >= 100) {
      setTrippyScene(10);
     }
   })
  },
  say: (message) => {
    // message = parser.output(message);
    twitchBot.say('#'+global.status.myChannel, message);
    logger.log(message);
  },
  broadcasterSay: (message) => {
    // message = parser.output(message);
    //twitchBroadcaster.say('#'+global.status.myChannel, message);
  }
}