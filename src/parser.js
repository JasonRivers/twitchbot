const https = require('https');

function output (message, userstate, args, c) {
    let tousername='';
    if (args && args[0]) {
	    tousername=args[0];
    } 
    if (args) {
      for (i=0; i<args.length; i++) {
        var regex = new RegExp('{arg' + i + '}', 'g');
        if (args[i].startsWith('@')) {
          tousername = args[i];
        }
        tousername = tousername.replace(regex, tousername.replace('@', ''));
        message = message.replace(regex, args[i].replace('@', ''));
      }
    }
    message = message.replace(/\$mychannel/g, global.config.myChannel.replace('#',''));
    if (userstate) {
      logger.log (userstate, 1);
      message = message.replace(/\$fromusername/g, userstate.username);
      message = message.replace(/\$tousername/g, tousername);
      message = message.replace(/\$touserid/g, userstate['user-id']);
      message = message.replace(/\$args/g, args.join(' '));
    }

    if (message.includes('readapi')) {

      apiRequest = message.replace(/.*\$readapi\((.*?)\).*/, '$1');

      apiRequest = apiRequest.replace(/https:\/\//, '');
      apiURL = apiRequest.replace(/\/.*/, '');
      apiPath = apiRequest.replace(apiURL, '');

      options = {
        hostname: apiURL,
        port: 443,
        path: apiPath,
        method: 'GET',
      }
      const req = https.request(options, (res) => {
        if (!res.statusCode === 200) {
          return false;
        } else {
          let chunks = [];
          res.on('data', (data) => {
            chunks.push(data);
          }).on('end', () => {
            let data = Buffer.concat(chunks);
            message = message.replace(/\$readapi\(.*\)/, data);
            c(message)
          })
        }
      })
      req.on('error', (e) => {
        logger.error(e);
      })
      req.end();
    } else {
      c(message);
    }

  }

//  setTimeout(() => {
//
//    var message="$tousername, you have been following $mychannel for $readapi(https://api.crunchprank.net/twitch/followage/$mychannel/$touserid?precision=3).";
//    message = message.replace(/\$mychannel/g, global.config.myChannel.replace('#',''));
//    console.log (message);
//  },5000);

  module.exports = {
    output
  }
