const fs = require('fs');

async function getData() {
  try {
    storage={};
    files = await fs.readdirSync('./storage/');
    for (i=0; i < files.length; i++) {
      if(files[i].includes('.json')) {
        storageName=files[i].replace('.json', '');
        storage[storageName]= await JSON.parse(fs.readFileSync('./storage/' + files[i]));
      }
    }
//    config.myChannel = config.twitch.twitchBot.channels[0];
    return(storage);

  } catch (err) {
    logger.error('Error occured while reading directory!', err);
  }
}

async function storeData(storage) {
  var storageFile = storage +'.json';
  var __storage = JSON.stringify(global.storage[storage], null, 2);
  try {
    fs.writeFileSync('./storage/' +storageFile, __storage, {
      encoding: 'utf8'
    });
  }
  catch (e) {
    logger.log(e);
  }
}

module.exports = {
  getData,
  storeData
}