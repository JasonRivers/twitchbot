const fs = require('fs');
module.exports = {
  loadModules
};

async function loadModules() {
  await fs.readdir('./src/modules/', (err, files) => {
    files.forEach(file => {
      moduleName=file.replace('.js', '');
      logger.log('*** Loading module: ' + moduleName);
      global.modules[moduleName] = require('./modules/' + moduleName);

      if (global.modules[moduleName].init) {
        logger.log('*** Initialising module: ' + moduleName);
        global.modules[moduleName].init();
      }

    })
  });
}

//module.exports = {
//  commands: require('./modules/builtinCommands'),
//  beatsaber: require('./modules/beatsaber'),
//  raffle: require('./modules/raffle'),
//  quotes: require('./modules/quotes'),
//  obs: require('./modules/obs'),
//  clipper: require('./modules/clipper')
//}
