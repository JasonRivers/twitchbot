const https = require('https');

function padWithLeadingZeros(string) {
  return new Array(5 - string.length).join("0") + string;
}

function unicodeCharEscape(charCode) {
  return "\\u" + padWithLeadingZeros(charCode.toString(16));
}

function unicodeEscape(string) {
  return string.split("")
    .map(function (char) {
        var charCode = char.charCodeAt(0);
        return charCode > 127 ? unicodeCharEscape(charCode) : char;
    })
    .join("");
}


function createPayload(data, conf){
  streamTitle = unicodeEscape(data.channel.status);
  regex = new RegExp('{channel}', 'g');
  postMessage = conf.postMessage.replace(regex, data.channel.display_name);
  payload = {
    "content": postMessage,
    "username": "Bot_Rainbow",
    "avatar_url": conf.avatar_url,
    "embeds": [{
      "title": streamTitle,
      "url": data.channel.url,
      "color": 6570404,
      "footer": {
        "text": data.channel.display_name
      },
      "image": {
        "url": data.preview.large,
      },
      "author": {
        "name": data.channel.display_name + " is now streaming!"
      },
      "fields": [
        {
          "name": "Playing",
          "value": data.channel.game,
          "inline": true
        },
        {
          "name": "Viewers",
          "value": data.viewers,
          "inline": true
        }
      ]
    }]
  };
  payload = JSON.stringify(payload);
  // stop JSON.stringify from messing up unicode characters
  payload = payload.replace(/\\\\u/g, '\\u');
  return (payload);

}


function postStream (stream, channel) {
    logger.log('SENDING DISCORD NOTICE', 1);
    if (global.app.config.twitch.twitchBot.channels[0].replace('#','').toLowerCase() === channel.replace('#','').toLowerCase()) {
      var discordConfig=global.app.config.discord.myChannel;
    } else {
      var discordConfig=global.app.config.discord.guestChannel;
    }
    if (discordConfig.postWhenLive) {
//      twitch.getChannel(channel, (data) => {
//        if (data) {
          var payload = createPayload(stream, discordConfig);

          options = {
            hostname: 'discordapp.com',
            port: 443,
            path: discordConfig.serverPath,
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Content-Length': payload.length
            }
          }

          var req = https.request(options, (res) => {
            logger.log('statusCode:' + res.statusCode, 1);
            logger.log('headers' + res.headers, 1);

            res.on('data', (d) => {
              process.stdout.write(d);
            });
          });
          req.on('error', (e) => {
            logger.log(e);
          });
          req.write(payload);
          req.end();
//        }
 //     })
    }
  }


function _init() {
  // channel timers
  var guestChannelTimer = global.config.discord.guestChannel.checkInterval * 1000;
  setInterval(() => {
    var channels = global.config.discord.guestChannel.channels;
    if (! global.status) {
      global.status = {};
    }
    if (!global.status.guestChannels) {
      global.status.guestChannels = {};
    }
    for (i=0; i<channels.length; i++) {
      const thisChannel = channels[i];
      if (!global.status.guestChannels[thisChannel]) {
        global.status.guestChannels[thisChannel] = true;
      }
      logger.log("Checking Stream " + thisChannel, 1);
      twitch.getChannel(thisChannel, (r) => {
        if (r.stream) {
          thisStreamStatus = true;
          logger.log(thisChannel + ' is live.', 1);
          if (global.status.guestChannel[thisChannel] !== thisStreamStatus && thisStreamStatus === true) {
            logger.log('Posting to Discord',1);
            postStream(r.stream,thisChannel);
            global.status.guestChannels[thisChannel] = true;
          }
        } else {
          logger.log(thisChannel + ' is offline.', 1);
          global.status.guestChannels[thisChannel] = false;
        }
      })
    }
  }, guestChannelTimer);
}

module.exports = {
  _init,
  postStream
}