const logger = require("../logger");

module.exports = {
    teamMembers: [],
    autoSO: [],
    shoutouts: [],
    init: () => {
        twitchapi.getTeamMembers('fevr', (members) => {
            module.exports.teamMembers = members;
            logger.log('Team Members:');
            logger.log(members);
        });
    },
    onChatHandler: (channel, userstate, message, self) => {
        if (module.exports.teamMembers.indexOf(userstate.username.toLowerCase())) {
            logger.log('found team member');
        }

    }
}