const permissions = require('../permissions');
const twitch = require('../twitch');

module.exports = {
  quotesTimer: 0,
  commands: (userstate, userBadges, command, args, c) => {
    if (command === "!quote") {
      switch (args[0]) {
        case 'add':
          if (permissions.checkPermissions(userBadges, ['moderator', 'broadcaster'])) {
            args.shift();
            message = args.join(' ');
            module.exports.addQuote(userstate, message, (ret) => {
              c(ret);
            return true;
            });
          }
          break;
        default:
          num = parseInt(args[0]) || 0;
          module.exports.getQuote(num, (ret) => {
            c(ret);
            return true;
          });
          break;
      }
    }
    return false
  },
  addQuote: (userstate, message, c) => {
    var d = new Date();
    var date = d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear();
    twitchapi.getGameFromUser(global.status.myChannel, (game) => {
      quote = {
        username: userstate.username,
        quote: message,
        game,
        date 
      }
      if (! global.storage) {
        global.storage = {}
      }
      if (! global.storage.quotes) {
        global.storage.quotes = [];
      }
      global.storage.quotes.push(quote);
      global.writeStorage('quotes');
      c('Quote [' + (global.storage.quotes.length) + '] - ' + quote.quote + ' added by ' + quote.username);
    })
  },
  getQuote: (quoteIndex=0, c) => {
    quotes = global.storage.quotes;

    if (quoteIndex===0) {
      quoteIndex = Math.floor(Math.random()*quotes.length);
    } else {
      quoteIndex = quoteIndex - 1;
    }
    if (quoteIndex < quotes.length) {
    quote = quotes[quoteIndex];
      c('Quote [' + (quoteIndex +1) + '] - ' + quote.quote + ' [' + quote.game + '] ' + quote.date + ' added by ' + quote.username);
    }
  },
  tick: () => {
    if (status.isLive) {
      module.exports.quotesTimer++; 
      if (module.exports.quotesTimer%20 === 0) {
        module.exports.getQuote(0, (quote) => {
          twitch.say(quote);

        })
      }
    }
  }
}