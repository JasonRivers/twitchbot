const https = require('https');
const fs = require('fs');
//const download = require('download-file');
const unzipper = require('unzipper');
const { getAudioDurationInSeconds } = require('get-audio-duration')


var isQueueOpen = false,
    songsinqueue = [];
    queueTimer = 0;
    fullQueueTimer = 0;
    maxQueueTime = 5400;
    autoClosed = false;

var download = function(url, dest, cb) {
  options = {
    hostname: 'beatsaver.com',
    port: 443,
    path: url,
    method: 'GET',
    headers: {
      'User-Agent': global.application.displayName + '/' + global.application.version,
    }
  }

  logger.log(options);
  var file = fs.createWriteStream(dest);
  var request = https.get(options, function(response) {
    response.pipe(file);
    file.on('finish', function() {
      logger.log('finished downloading');
      file.close(cb);  // close() is async, call cb after close completes.
    });
  }).on('error', function(err) { // Handle errors
      logger.error(err);
    fs.unlink(dest); // Delete the file async. (But we don't check the result)
    if (cb) cb(err.message);
  });
};


module.exports = {
    directory: './beatmaps',
    init: () => {
      // start with a closed queue
      isQueueOpen = false;
      // clear the songs in queue
      songsInQueue = [];
      if (!fs.existsSync(module.exports.directory)) {
        fs.mkdirSync(module.exports.directory);
      }
    },
    getQueueTime: (c) => {
        var queueTime = {
            hours: 0,
            minutes: 0
        }
            //uptimeSeconds = (new Date().getTime() / 1000) - global.status.startTime;
            queueTime.hours = Math.floor(queueTimer / 3600);
            queueTime.minutes = Math.floor(queueTimer % 3600 / 60);
        c(queueTime);
    },
    checkQueueTime: (c) => {
        if (queueTimer >= maxQueueTime) {
            if (isQueueOpen && !autoClosed) {
          logger.log ('Timer is: ' + queueTimer + ' of ' + maxQueueTime)
                twitch.say('Max queue time has been reached, Auto-closing the queue');
                isQueueOpen = false;
                twitch.say('!close');
                autoClosed = true;
            }
        } else {
          logger.log ('Timer is: ' + queueTimer + ' of ' + maxQueueTime)
        }
    },
    checkBeatmap: (beatmap, c) => {
      options = {
        hostname: 'beatsaver.com',
        port: 443,
        path: '/api/maps/id/' + beatmap,
        method: 'GET',
        headers: {
          'User-Agent': global.application.displayName + '/' + global.application.version,
          'Content-Type': 'application/json',
        }
      }
      const req = https.request(options, (res) => {
        if (!res.statusCode === 200) {
          c(false);
        } else {
          let chunks = [];
          res.on('data', function(data) {
            chunks.push(data);
          }).on('end', function() {
            let newdata   = Buffer.concat(chunks);
            try {

              let schema = JSON.parse(newdata);
              c(schema);
            }
            catch (e){
              logger.error(e);
              c(false);
            }
          })
        }
      });
      req.on('error', (e) => {
        logger.error(e);
      });
      req.end();
    },
    commands: (userstate, userBadges, command, args, c) => {
        if ((command === "!bsr" || command === "!modadd" || command ==="!add" || command === "!att") && (args[0] !== command)) {
          if (isQueueOpen) {
              if (songsInQueue.indexOf(args[0]) === -1) {

                songsInQueue.push(args[0]);
                module.exports.checkBeatmap(args[0], (ret) => {
                    var beatmapKey = ret.key;
                    if (ret && ret.metadata) {
                        logger.log('Request: ' + ret.metadata.songName + ' - ' + ret.metadata.songAuthorName);
//                        if (ret.metadata.duration !== 0) {
//                          return true;
                            logger.info('no Duration for ' + ret.key)
                            console.log(ret)
                            clipURL= ret.versions[0].downloadURL;
                            console.log(clipURL);
                            logger.info('Downloading file from beatsaver to add duration:' + clipURL)
                            file_path= module.exports.directory + '/zip/' + ret.key + '.zip';
                            download(clipURL, file_path, (e) => {
                              logger.log('clip downloaded');
                                fs.readdir(module.exports.directory + '/zip', (err, files) => {
                                    files.forEach(file => {
                                      fs.mkdir(module.exports.directory + '/' + beatmapKey, () => {

                                      fs.createReadStream(module.exports.directory + '/zip/' + file)
                                        .pipe(unzipper.Extract({path:module.exports.directory + '/' + beatmapKey + '/'}))
                                        .promise()
                                        .then(() => {
                                            fs.unlinkSync(module.exports.directory + '/zip/' + file);
                                            fs.readdir(module.exports.directory + '/' + beatmapKey + '/', (err, files) => {
                                                files.forEach((f) => {
                                                    if (f.endsWith('.egg')) {
                                                        getAudioDurationInSeconds(module.exports.directory + '/' + beatmapKey + '/' + f).then((duration) => {
                                                            queueTimer = queueTimer + Math.floor(duration)
                                                            module.exports.getQueueTime(currentQueueTime => {
                                                                module.exports.checkQueueTime();
                                                            })
                                                            fs.rmdirSync(module.exports.directory + '/' + beatmapKey, {recursive: true});
                                                        })
                                                    }
                                                })
                                            });
                                        })
                                      })
                                    });
                                  });
                                logger.log ('finished');
                            })
                          /*                        } else {
                            queueTimer = queueTimer + ret.metadata.duration; 
                            module.exports.getQueueTime(currentQueueTime => {
                                logger.log('Current Queue Time');
                                module.exports.checkQueueTime();
                            })
                        } */
                    }
                })
            } else {
              logger.log('song already requested');
            }
          }
        }
        switch (command) {
            case "Queue":
              if (args && args[2]) {
                switch(args[2]) {
                  case "closed.":
                    isQueueOpen = false;
                logger.log('closing queue')
                    break;
                  case "open.":
                    isQueueOpen = true;
                logger.log('opening queue')
                    break;
                }
              }
              break;
            case "!open":
                logger.log('opening queue')
                isQueueOpen = true;
                break;
            case "!close":
                logger.log('closing queue')
                isQueueOpen = false;
                break;
        }
    }

}