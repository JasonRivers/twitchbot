const OBSWebSocket = require('obs-websocket-js');
const obs = new OBSWebSocket();

var dcTimer;

global.obsFirstRun = true;

global.obsConnected = false

obs.on('ConnectionOpened', (d) => {
    obs.sendCallback('GetVersion', (e,data) => {
        if (!e) {
          logger.log('*** Connected to OBS-'+data['obs-studio-version'] + ' ***');
          logger.log('*** Available OBS Scenes ***');
          obs.sendCallback('GetSceneList', (e,data) => {
            if (!e) {
            s='';
            for(i=0;i<data.scenes.length;i++){
              s = s + "\n" +  data.scenes[i].name ;
            }
            logger.log(s);
            } else {
              logger.error(e);
            }
          });
//          module.exports.setItemVisible('Render - Trip', 'Trip-NDI', true, () => {
//          module.exports.getSceneItemProperties('Render - Trip', 'Trip-NDI', (d) => {
//          obs.sendCallback('GetSceneItemProperties', {'scene-name': 'Render - Trip', item: 'Trip-NDI'},(e, d) => {
//            logger.log(d);
//          })
        } else {
            logger.log(e);
        }
    });
});

obs.on('ConnectionClosed', (data) => {
  if (global.obsConnected === true) {
    logger.log('*** OBS Disconnected ***');
    global.obsConnected = false;
  }
  setTimeout(() => {
    module.exports.init();
  },10000);
});


module.exports = {
  init: () => {
    if (global.obsFirstRun) {
      logger.log('***Connecting to OBS***');
    }
      obs.connect({
        address: global.app.config.obs.address + ':' + global.app.config.obs.port,
        password: global.app.config.obs.password
      })
      .then(() => {
        global.obsConnected = true;
      })
      .catch((e) => {
        if (global.obsFirstRun) {
          logger.error('Unable to connect to OBS, Is it running?');
          global.obsFirstRun = false;
        }
      })
  },
  getVersion: (f) => {
    if (obsConnected) {
      obs.sendCallback('GetVersion', (e,data) => {
        if (!e) {
          f(data);
        } else {
          logger.error(e);
        }
      });
    }
  },
  getScene: (f) => {
    obs.sendCallback('GetCurrentScene', (e, data) => {
      if (!e) {
        f(data.name);
      }
    });
  },
  getScenes: (f) => {
    obs.sendCallback('GetSceneList', (e,data) => {
      if (!e) {
        scenes = '';
        for (i=0; i< data.scenes.length; i ++)  {
          if (scenes === '') {
            scenes = data.scenes[i].name;
          } else {
            scenes = scenes +', '+data.scenes[i].name;
          }
        }
        f(scenes);
      } else {
        logger.error(e);
      }
    });
  },
  setScene: (scene, f) => {
    obs.sendCallback('SetCurrentScene', {'scene-name':scene}, (e,data) => {
        if (!e) {
          if (f) {
            f(data,scene);
          } else {
            return;
          }
        } else {
          logger.error(e);
        }
      });
  },
  processCommand: (command) => {
    //command should be an object from the config file
    if (command.obsActiveSources) {
      logger.log('Activating OBS Sources');
      //activate Sources
      for(i=0;i<command.obsActiveSources.length;i++) {
        enableSource(command.obsActiveSources[i],command.obsScene);
      }
      for(i=0;i<command.obsInactiveSources.length;i++) {
        disableSource(command.obsInactiveSources[i],command.obsScene);
      }
    }
    else if (command.obsToggleSource) {
      // Toggle the obsSource
      logger.log("Toggling visibility of " + command.obsToggleSource);
      if (command.obsScene) {
        toggleSource(command.obsToggleSource, command.obsScene);
      } else {
        obs.getScene((scene) => {
          toggleSource(command.obsToggleSource, scene);
        });
      }
    } else {
      logger.log("Switching OBS Scene to " + command.obsScene);
      setScene(command.obsScene, (r, scene) => {
        if (r.status === "ok"){
          logger.log("Successfully switched OBS Scene to " + scene);
        }
      });
    }
  },
  getSourcesList: (c) => {
    obs.sendCallback('GetSourcesList', (e,data) => {
      c(data);
    })
  },
  getSourceSettings: (sourceName, c) => {
    obs.sendCallback('GetSourceSettings', {sourceName}, (e,data) => {
      c(data);
    })
  },
  setSourceSettings: (sourceName, sourceSettings, c) => {
    obs.sendCallback('SetSourceSettings', {sourceName, sourceSettings}, (e,data) => {
      if (c) {
        c(data);
      }
    })
  },
  disconnect: () => {
    obs.disconnect();
  },
  getSceneItemProperties: (scene, item, c) => {
    obs.sendCallback('GetSceneItemProperties', {'scene-name': scene, item}, (e, data) => {
      if (c) {
        c(data);
      }
    })
  },
  setSceneItemProperties: (scene, item, c) => {
    obs.sendCallback('SetSceneItemProperties', {'scene-name': scene, item, visible: false }, (e, data) => {
      if (c) {
        c(data);
      }
    })
  },
  setItemVisible: (scene, item, visible, c) => {
    obs.sendCallback('SetSceneItemProperties', {'scene-name': scene, item, visible }, (e) => {
      if (c) {
        c();
      }
    })
  }
}
