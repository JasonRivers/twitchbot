const permissions = require('../permissions');

var raffleUsers = [];
var raffleOpen = false;

module.exports = {
  commands: (userstate, userBadges, command, args, c) => {
    if (command === "!raffle") {
      if (!args[0] || args[0] === command) {
        c('To enter the raffle type raffle without the ! - Dummy!');
        return false;
      }
//      //Moderator
      if (permissions.checkPermissions(userBadges, ['moderator', 'broadcaster'])) {
        switch (args[0]) {
          case 'open':
            module.exports.open();
            break;
          case 'close':
            module.exports.close();
            break;
          case 'reset':
            module.exports.reset();
            break;
          case 'draw':
            module.exports.draw();
            break;
          case 'adduser':
           module.exports.draw(args[1], userstate)
            break;
        }
      }
    }
    // User
    if (command === "raffle") {
      module.exports.addUser(userstate.username, userstate);
    }
  },
  addUser: (username, userstate) => {
    if (raffleOpen) {
      if (Object.keys(userstate.badges)) {
        userBadges = Object.keys(userstate.badges);
      } else {
        userBadges = [];
      }
      logger.log('adding user');
      if (raffleUsers.indexOf(username.toLowerCase()) === -1) {
        //add the user.
        raffleUsers.push(username.toLowerCase());
        //Subs get doube chance
        if (userBadges.indexOf('subscriber') !== -1 || userBadges.indexOf('founder') !== -1) {
          raffleUsers.push(username.toLowerCase());
        }
      }
      logger.log(raffleUsers);
    }

  },
  draw: () => {
    var winner = raffleUsers[Math.floor(Math.random()*raffleUsers.length)];
    c(winner + ' Has won the raffle');
  },
  reset: () => {
    raffleUsers=[];
    c('Raffle is now Reset');
  },
  open: () => {
    logger.log('opening');
    raffleOpen=true;
    c('Raffle is now open');
  },
  close: () => {
    raffleOpen=false;
    c('Raffle is now Closed');
  }
}