const https = require('https');
const ObsWebSocket = require('obs-websocket-js');

module.exports = {
  state: '',
  apiRequest: (request, c) => {
    options = {
      hostname: 'octoprint.jasonrivers.co.uk',
      port: 443,
      path: '/api/' + request,
      method: 'GET',
      headers: {
        'User-Agent': global.application.displayName + '/' + global.application.version,
        'Content-Type': 'application/json',
        'Authorization': 'Bearer 3610D11A753E41A785C3ADF276AF95B8'
      }
    }
    const req = https.request(options, (res) => {
      if (!res.statusCode === 200) {
        c(false);
      } else {
        let chunks = [];
        res.on('data', function(data) {
          chunks.push(data);
        }).on('end', function() {
          let newdata   = Buffer.concat(chunks);
          try {
            let schema = JSON.parse(newdata);
            c(schema);
          }
          catch (e){
            logger.error(e);
            c(false);
          }
        })
      }
    });
    req.on('error', (e) => {
      logger.error(e);
    });
    req.end();
  },
  getCurrentJob: () => {
    module.exports.apiRequest('job', (d) => {
      if (d.state !== module.exports.state) {
       if (d.state === "Printing") {
        twitch.say('3D Print started, use !printer to find out what\'s happening!');
       } else {
        global.modules.obs.setItemVisible('Printer', 'Octoprint', false);
       }
      }
      module.exports.state = d.state;
    })
  },
  commands: (userstate, userBadges, command, args, c) => {
    switch (command) {
      case "!printer":
        module.exports.apiRequest('job', (d) => {
          if (d.state === "Printing") {
            c('Printer is currently printing '+d.job.file.name.replace('.gcode', '') + ' (' + Math.floor(d.progress.completion) + '% complete) - Show the printer with !showprinter');
          } else {
            c('Nothing is printing currently');
          }
        });
        break;
      case "!hideprinter":
        global.modules.obs.setItemVisible('Printer', 'Octoprint', false);
        break;
      case "!showprinter":
        module.exports.apiRequest('job', (d) => {
          if (d.state === "Printing") {
            global.modules.obs.setItemVisible('Printer', 'Octoprint', true);
          } else {
            c('Nothing is printing currently');
          }
        });
        break;
    }

  },
  tick: () => {
    module.exports.getCurrentJob();
  }
}
