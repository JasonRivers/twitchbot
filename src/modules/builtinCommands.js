const logger = require('../logger');
const permissions = require('../permissions');
const status = require('../streamstatus');

module.exports = {
  commands: (userstate, userBadges, command, args, c) => {
    //moderator commands
    if (permissions.checkPermissions(userBadges, ['moderator', 'broadcaster'])) {
      switch (command) {
        case "!command":
          var action = args[0];
          args.shift();
          var commandName = args[0];
          args.shift();
          var actionPermissions = args[0];
          args.shift();
          var commandPermissions =[];
          switch(action) {
            case 'add':
            case 'edit':
             // permisisions:
             /*
             +a: everyone
             +s: subscriber
             +m: moderator
             +b: broadcaster
             */
            switch (actionPermissions) {
              case '+a':
                commandPermissions=['everyone'];
                break;
              case '+s':
                commandPermissions=['subscriber', 'founder', 'moderator', 'broadcaster'];
                break;
              case '+m':
                commandPermissions=['moderator', 'broadcaster'];
                break;
              case '+b':
                commandPermissions=['broadcaster'];
                break;
              default:
                c('you\'re a muppet! - Add/edit command: !command [add|edit|remove] {command} ({permissions} {message})');
                return;
                break;
            }
            commandResponse=args.join(' ');
            
            newCommand = {
              permissions: commandPermissions,
              response: commandResponse,
              command: commandName.toLowerCase()
            }
            if (!global.app.config.commands) {
              global.app.config.commands = {}
            }
            global.app.config.commands[commandName.toLowerCase()] = newCommand;
            // we need to save this somewhere
            global.writeConfig('commands');
            if (action === 'add') {
              c('command ' + commandName + ' has been added!');
            } else if (action === 'edit') {
              c('command ' + commandName + ' has been modified!');
            }

            break;
            case 'remove':
            break;
          }
          break;
        case "!addso":
          var user;
          if (args[0].startsWith('@')) {
            user = args[0].replace('@', '');
          } else {
            user = args[0];
          }
          args.shift();
          soMessage=args.join(' ');
          if (!global.app.config.shoutouts) {
            global.app.config.shoutouts={};
          }
          if (!global.app.config.shoutouts.customShoutout) {
            global.app.config.shoutouts.customShoutout={};
          }
          global.app.config.shoutouts.customShoutout[user.toLowerCase()] = soMessage;
          global.writeConfig('shoutouts');

          c('Custom shoutout for ' + user + ' has been added');
          break;
        case "!so":
          var user;
          user = args[0].replace('@', '').toLowerCase();
          twitch.getGameFromUser(user, (game) => {
            sousers = Object.keys (global.app.config.shoutouts.customShoutout);
            if (sousers.indexOf(user.toLowerCase()) > -1) {
              logger.log(global.app.config.shoutouts.customShoutout[user]);
              msg = global.app.config.shoutouts.customShoutout[user];
              msg = msg.replace('$game', game);
              msg = msg.replace('$twitchurl', 'https://twitch.tv/' + user);
            } else {
                msg = 'Go checkout ' + user + ' who was last playing ' + game +' here: https://twitch.tv/' + user;
            }
            c(msg);
          })
          break;
      }
    }

    //standard user commands
    switch (command) {
      case "!ping": 
        c('pong');
        break;
      case "!uptime":
        if (global.status.isLive) {
          status.uptime((ret) => {
            if (ret.hours === 0 ) {
              c('Stream has been live for ' + ret.minutes.toString().padStart(2, '0') + ' minutes!');
            } else {
              c('Stream has been live for ' + ret.hours + ' hours and ' + ret.minutes.toString().padStart(2, "0") + ' minutes!');
            }
          })
        } else {
          c('Stream is currently offline');
        }
        break;
      case "!time":
        t = new Date();
        c('Mobeardo\'s local time is ' + t.getHours() + ':' + t.getMinutes().toString().padStart(2, 0));
        break;
    }

    // Custom commands
    if (global.app.config.commands && global.app.config.commands[command]) {
      if (permissions.checkPermissions(userBadges, global.app.config.commands[command].permissions)) {
        c(global.app.config.commands[command].response)
      }
    }

  },
  init: () => {
    if (global.app.config.commands) {
      logger.log('Custom commands:');
      logger.log(Object.keys(global.app.config.commands));
    }
  }
}