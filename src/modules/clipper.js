const download = require('download-file');

var streamclips = [];
var clipsLength = 0

module.exports = {
  init: () => {
    if (modules.obs && global.obsConnected) {
      modules.obs.getSourcesList((data) => {
        logger.log('-- OBS Sources --');
        logger.log(data);
      });
    } else {
      setTimeout(() => {
        module.exports.init();
      }, 500);
    }
  },
    directory: global.app.config.clipper.clipsDirectory,
    downloadTodayClips: (username, c) => {
        d = new Date();
        d.setDate(d.getDate() -global.app.config.clipper.clipsDays);
        startDate = d.toISOString();
        twitchapi.getTwitchClips({username, startdate: startDate}, (clips) => {
            for (var i=0; i<clips.data.length; i++) {
              if (clips.data[i].title !== status.title) {
                if (streamclips.indexOf(clips.data[i].id) === -1) {
                    streamclips.push(clips.data[i].id);
                    twitch.say(clips.data[i].creator_name + ', Thanks for clipping the stream! ' + clips.data[i].url);
                    var clipURL = clips.data[i].thumbnail_url.replace(/-preview.*/, '.mp4');
                    var dl_options = {
                        directory: module.exports.directory,
                        filename: clips.data[i].id + '_' + clips.data[i].creator_name + '.mp4'
                    }
                    download(clipURL, dl_options, (e) => {
                        if (e) {
                            logger.error(e);
                        }
                        logger.log('Clip '+clipURL+' Downloaded to '+ dl_options.directory);
                    });
                }
              }
            }
            c(clips);
        });
    },
    tick: () => {
//      if (status.isLive) {
        logger.debug(global.status.myChannel);
        module.exports.downloadTodayClips(global.status.myChannel, () => {
          if (streamclips.length !== clipsLength) {
            setTimeout(() => {
              if (global.obsConnected) {
                modules.obs.getSourcesList((data) => {
                  for (var i=0; i<data.sources.length; i++) {
                    if (data.sources[i].name === "Highlights") {
                      modules.obs.setSourceSettings(data.sources[i].name, {
                        playlist: [{
                          hidden: false,
                          selected: true,
                          value: global.app.config.clipper.clipsDirectory
                        }]
                      })
                    }
                  }
                })
              }
              clipsLength = streamclips.length;
            }, 1000)
          }
        })
      }
 //   }
}


// https://clips.twitch.tv/OilyTenderLardRickroll-eGIrcfyGxQpOt5Ad