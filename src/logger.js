const winston = require('winston');
const { name, displayName, version } = require('../package.json');

module.exports = { 
    logger: null,
    init:() => {
        console.log("running logger at level: " +  global.app.config.logging.logLevel);
        this.logger = winston.createLogger({
            level: global.app.config.logging.logLevel,
            format: winston.format.combine(
                winston.format.timestamp({
                    format: 'YYYY-MM-DD HH:mm:ss'
                }),
                winston.format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`+(info.splat!==undefined?`${info.splat}`:" "))
            ), 
            transports: [
                new winston.transports.Console(),
                new winston.transports.File({ filename: global.app.config.logging.logPath + '/error.log', level: 'error', options: { flags: 'w' } }),
                new winston.transports.File({ filename: global.app.config.logging.logPath + '/' + global.app.name + '.log', options: { flags: 'w'} })
//                new winston.transports.Http({
//                    host: 'localhost',
//                    port: global.app.config.webserver.port,
//                    path: '/logcollector'
//                })
            ]
        });
    },
    debug: (msg) => {
        if ((typeof msg === "string") || ( typeof msg === "number") || (typeof msg === "boolean")) {
        logmsg=msg;
        } else {
        logmsg=JSON.stringify(msg, null, 4);
        }
        data = {
            level: 'debug',
            message: logmsg
        }
        //console.log('[' + data.level + '] ' + data.message);
        this.logger.log(data);
    },
    log: (msg) => {
        if ((typeof msg === "string") || ( typeof msg === "number") || (typeof msg === "boolean")) {
            logmsg=msg;
        } else {
            logmsg=JSON.stringify(msg, null, 4);
        }
        data = {
            level: 'info',
            message: logmsg
        };
        //console.log('[' + data.level + '] ' + data.message);
        if (this.logger.log) {
            this.logger.log(data);
        } else {
            console.log(data);
        }
    },
    error: (msg) => {
        if ((typeof msg === "string") || ( typeof msg === "number") || (typeof msg === "boolean")) {
            logmsg=msg;
        } else {
            logmsg=JSON.stringify(msg, null, 4);
        }
        data = {
            level: 'error',
            message: logmsg
        };
        //console.log('[' + data.level + '] ' + data.message);
        this.logger.log(data);
    },
  logApplicationInfo: () => {
    var application = {
        name,
        version: getVersionNumber(),
        displayName
    }
    global.application = application;
  },
}

function getVersionNumber() {
    revision = require('child_process')
  .execSync('git rev-list --count HEAD')
  .toString().trim()
  full_version=version.replace(new RegExp('0$'), revision);
  return full_version;
};