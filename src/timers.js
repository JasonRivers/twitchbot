const parser = require('../parser');

var noticeTimer,
    gameTimer;

var timers = {
  noticeTimer:() => {
//    var timers = global.config.timers.timers;
    let timers = global.activeTimers;
    let timersKey = Object.keys(timers);
    currentTimer=0;
    logger.log('RUNNUNG NOTICE TIMER', 1);
    if (global.streamStatus && global.config.timers.settings.noticeTimerEnabled){
      //if (global.config.timers.settings.noticeTimerEnabled){
      parser.output(timers[timersKey[currentTimer]].response, null, null, (message) => {
//        twitch.say(message);
	logger.debug(message);
      });
    }
    currentTimer++;
    if (currentTimer >= timersKey.length) {
      refreshTimers();
      timers = global.activeTimers;
      timersKey = Object.keys(timers);
      console.log(timersKey);
      currentTimer=0;
    }
  }
}

function setActiveTimers() {
  // remove disabled timers
  timers = global.config.timers.timers;
  timerKeys=Object.keys(timers);
  let newTimers ={};
  for (var i=0; i<timerKeys.length; i++) {
    if (timers[timerKeys[i]] && timers[timerKeys[i]].enabled && timers[timerKeys[i]].enabled === true) {
      newTimers[timerKeys[i]] = timers[timerKeys[i]];
    }
  }
  global.activeTimers=newTimers;
}

async function refreshTimers() {
      await global.reloadConfigFromFile('timers');
      setActiveTimers();
}

function noticeTimer()  {
//    var timers = global.config.timers.timers;
    var timers = global.activeTimers;
    var timersKey = Object.keys(timers);
    currentTimer=0;
    noticeTimer = setInterval(function(){
      logger.log('RUNNUNG NOTICE TIMER', 1);
      if (global.streamStatus && global.config.timers.settings.noticeTimerEnabled){
      //if (global.config.timers.settings.noticeTimerEnabled){
        parser.output(timers[timersKey[currentTimer]].response, null, null, (message) => {
          twitch.say(message);
        });
      }
      currentTimer++;
      if (currentTimer >= timersKey.length) {
        refreshTimers();
        timers = global.activeTimers;
        timersKey = Object.keys(timers);
        console.log(timersKey);
        currentTimer=0;
      }
    }, global.config.timers.settings.noticeTimer);
}

function gameLinkTimer() {
  gameTimer = setInterval(() => {
    logger.log ("RUNNING GAME LINK TIMER", 1);
    twitch.getGame(global.config.myChannel,(game) => {
        if (global.config.timers.games[game]) {
          logger.log("found game", 1);
          msg = "If you like {game}, you can pick it from here: {url}";
          msg = msg.replace('{game}',game);
          msg = msg.replace('{url}',global.config.timers.games[game].url);
          if (global.streamStatus && global.config.timers.settings.gameLinkTimerEnabled){
            twitch.say(msg);
          }
        }
    })
  }, global.config.timers.settings.gameLinkTimer);
}

module.exports = {
  init: () => {
    setActiveTimers();
    //start general timer
    noticeTimer();
    //start the Game Timer
//    gameLinkTimer();
    //start Timers that are always active
  },
  commands: (userstate, command, args, c) => {
    //commands for timers
    var timers = global.config.timers.timers;
  }
};
