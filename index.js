const _pkg = require('./package.json');
const _env = require('dotenv');
const config = require('./src/config.js');
const storage = require('./src/storage.js');
const _logger = require('./src/logger.js');
const status = require('./src/streamstatus.js');
const _twitch = require('./src/twitch.js');
const _twitchapi = require('./src/twitchapi.js');
const _modules = require('./src/modules');
const _oauth = require('./src/oauth');

_env.config()

//Setup Globals

//default status
global.status = {
  isLive: false,
  startTime: 0,
  game: '',
  title: ''
}
global.twitch = _twitch;
global.twitchapi = _twitchapi;
global.modules = _modules;
global.writeConfig = config.storeConfig;
global.writeStorage = storage.storeData;
global.oauth = _oauth;
global.logger = _logger;

global.app = {
  version: _pkg.version,
  copyright: 'Copyright © ' + _pkg.author.name + ' (' + _pkg.author.email + ')',
  name: _pkg.name,
  displayName: _pkg.displayName,
  author: _pkg.author,
  description: _pkg.description,
  tickTimer: 0,
  configPath: './config',
  config: {
    logging: {
      logLevel: "debug",
      logPath: '.'
    },
    webserver: {
      port: 6501
    },
    twitch: {},
    obs: {
      address: 'localhost',
      port: '4444',
    },
    clipper: {
      clipsDirectory: '/home/jason/Videos/Highlights',
      clipsDays: 1
    }
  },
  boot: async () => {
    let loadedConfig = await config.getConfig();
    global.logger.init();
    loadedConfigNames=Object.keys(loadedConfig);
    for (var i=0; i<loadedConfigNames.length; i++) {
      global.app.config[loadedConfigNames[i]] = loadedConfig[loadedConfigNames[i]];
    }
    global.storage = await storage.getData();
    await logger.logApplicationInfo();

    global.oauth.init();
    //check twitch auth
    global.oauth.checkToken('twitch', () => {
      global.app.init();
    });
  },
  init: async () => {
    console.log(global.app.config);
    global.twitch.init();
    logger.log('Loading Modules');
    await global.modules.loadModules();
    setTimeout(() => {
      global.app.timerLoop();
    },1000);
    // run the loop every minute
    setInterval(app.timerLoop,60000);
  },
  timerLoop: () => {
    logger.debug('TickTimer');
    var oldStatus = global.status.isLive;

    //set the stream status
    status.stream((ret) => {
      if (ret === true && oldStatus !== ret) {
        app.streamStart();
      } else if (ret === false && oldStatus !== ret) {
        app.streamStop();
      }
    });

    status.game(global.status.myChannel,(ret) => {
      global.status.game = ret.game;
    })
    status.getStreamTitle((status) => {
      global.status.title = status;
    })

    moduleNames = Object.keys(global.modules);
    for (i=0; i<moduleNames.length; i++) {
      if (global.modules[moduleNames[i]].tick) {
        global.modules[moduleNames[i]].tick();
      }
    }

  },
  streamStart: () => {
    logger.log("==== Stream Now Online ====");
    // Set the stream start time
    global.status.startTime = new Date().getTime() / 1000;
    // Run module stuf for stream starting

    moduleNames = Object.keys(global.modules);
    for (i=0; i<moduleNames.length; i++) {
      if (global.modules[moduleNames[i]].onStreamStart) {
        global.modules[moduleNames[i]].onStreamStart();
      }
    }

  },
  streamStop: () => {
    logger.log("==== Stream Now Offline ====");
            uptimeSeconds = (new Date().getTime() / 1000) - global.status.startTime;
            uptimeHours = Math.floor(uptimeSeconds / 3600);
            uptimeMinutes = Math.floor(uptimeSeconds % 3600 / 60);

    moduleNames = Object.keys(global.modules);
    for (i=0; i<moduleNames.length; i++) {
      if (global.modules[moduleNames[i]].onStreamEnd) {
        global.modules[moduleNames[i]].onStreamEnd();
      }
    }
  }
}



global.app.boot();